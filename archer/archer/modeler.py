import collections
import fnmatch
import yaml


def build(hub):
    """
    Take the coompiled data and build the model
    """
    hub.archer.modeler.counts()
    hub.archer.modeler.macros()

def check_skip(hub, pathref):
    for pattern in hub.OPT.archer.ignore:
        if fnmatch.fnmatch(pathref, pattern):
            return True
    return False


def counts(hub):
    """
    Populate the counts
    """
    ignore_keys = ("__id__", "__sls__", "fun", "state", "order")
    hub.archer.COUNTS = {}
    hub.archer.COMMON = {}
    for chunk in hub.archer.COMPILED["low"]:
        pathref = f"{chunk['state']}.{chunk['fun']}"
        if hub.archer.modeler.check_skip(pathref):
            continue
        if pathref not in hub.archer.COUNTS:
            hub.archer.COUNTS[pathref] = {}
        # Populate counts
        for key, val in chunk.items():
            if key in ignore_keys:
                continue
            if not isinstance(val, collections.Hashable):
                val = yaml.dump(val)
            if key not in hub.archer.COMMON:
                hub.archer.COMMON[key] = {"instances": 1, "vals": [val], "pathrefs": set([pathref])}
            else:
                hub.archer.COMMON[key]["instances"] += 1
                hub.archer.COMMON[key]["vals"].append(val)
                hub.archer.COMMON[key]["pathrefs"].add(pathref)
            if key not in hub.archer.COUNTS[pathref]:
                hub.archer.COUNTS[pathref][key] = {}
            if val not in hub.archer.COUNTS[pathref][key]:
                hub.archer.COUNTS[pathref][key][val] = 1
                continue
            else:
                hub.archer.COUNTS[pathref][key][val] += 1


def macros(hub):
    """
    Populate the raw macros data
    """
    hub.archer.MACROS = {}
    hub.archer.VARS = {}
    for pathref, vals in hub.archer.COUNTS.items():
        if pathref not in hub.archer.MACROS:
            hub.archer.MACROS[pathref] = {}
            hub.archer.VARS[pathref] = set()
        for key, count in vals.items():
            for val, num in count.items():
                if num == 1:
                    # Always the same, no variable needed
                    hub.archer.MACROS[pathref][key] = val
                else:
                    hub.archer.MACROS[pathref][key] = "__variable__"
                    hub.archer.VARS[pathref].add(key)
