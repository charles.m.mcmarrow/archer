# Import python libs
import pprint


def __init__(hub):
    # Remember not to start your app in the __init__ function
    # This function should just be used to set up the plugin subsystem
    # The run.py is where your app should usually start
    for dyne in ["idem"]:
        hub.pop.sub.add(dyne_name=dyne)


def cli(hub):
    hub.pop.config.load(["archer"], cli="archer")
    # Your app's options can now be found under hub.OPT.archer
    kwargs = dict(hub.OPT.archer)

    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Start the async code
    coroutine = hub.archer.init.run(**kwargs)
    hub.pop.Loop.run_until_complete(coroutine)


async def run(hub, **kwargs):
    """
    This is the entrypoint for the async code in your project
    """
    hub.archer.COMPILED = await hub.archer.compiler.compile()
    hub.archer.modeler.build()
    #print("COMPILED")
    #print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    #pprint.pprint(hub.archer.COMPILED)
    print("VARS")
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    pprint.pprint(hub.archer.VARS)
    print("MACROS")
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    pprint.pprint(hub.archer.MACROS)
    print("COUNTS")
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    pprint.pprint(hub.archer.COUNTS)
    print("COMMON")
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    pprint.pprint(hub.archer.COMMON)
